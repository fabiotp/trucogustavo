from random import shuffle
import sys

naipes = ['Espadas','Copas','Paus','Ouros']

def welcomeTruco():
    print('''Bem-vindo ao Truco em Python!        
created by: Gustavo Lira''')
    jumpLine()
    print('''[1] Jogar
[2] Regras
[3] Sair do Programa''')
    menu = 0
    while menu != 3:
        menu = int(input('>>>> Qual é a sua opção?: '))
        if menu == 1: break
        if menu == 2: print('Vo botar as regras aqui')
        if menu == 3: sys.exit('Faloou')

def generateCardDeck():
    cards = []
    numbers = [4,5,6,7,'Q','J','K','A',2,3]

    for naipe in naipes:
        for number in numbers:
            card = (f'{number} de {naipe}')
            cards.append(card)
    return cards

def generateShuffleDeck():
    deck = generateCardDeck()
    shuffle(deck)
    return deck

def players(hand):
    playerone = hand[0:3]
    playertwo = hand[3:6]
    playerthree = hand[6:9]
    playerfour = hand[9:12]
    return playerone, playertwo, playerthree, playerfour

def choiceCard(hand, deck):
    try:
        pos = int(input('Escolha uma carta [1-3]: ')) - 1
        deck.append(hand[pos])
        jumpLine()
        return print('Cartas na mesa:', deck)
    except: print('Algo deu errado')

def gameMatch():
    playerone, playertwo, playerthree, playerfour = players(generateShuffleDeck())
    jumpLine()

    print('Primeiro Jogador:')
    print(playerone)
    choiceCard(playerone, list)
    jumpLine()

    print('Segundo Jogador:')
    print(playertwo)
    choiceCard(playertwo, list)
    jumpLine()

    print('Terceiro Jogador:')
    print(playerthree)
    choiceCard(playerthree, list)
    jumpLine()

    print('Quarto Jogador:')
    print(playerfour)
    choiceCard(playerfour, list)
    jumpLine()

    compareCards(list)  #Adicionei esta funcao


def compareCards(deck):
    # Cria 1 lista para cada jogador, contendo toda a informacao necessaria da carta jogada, numero + naipe(so preciso da primeira letra pra saber o naipe)
    # Falta implementar a comparacao dos naipes, mas eu nao sei jogar esse truco entao vai ficar assim por enquanto ...
    # A formatacao do deck esta da seguinte forma: 'x de Naipe' , x sempre esta no index 0 e a primeira letra do naipe sempre esta no index 5
    p1 = [deck[0][0] , deck[0][5]]
    p2 = [deck[1][0] , deck[1][5]]
    p3 = [deck[2][0] , deck[2][5]]
    p4 = [deck[3][0] , deck[3][5]]
    
    print(p1)
    print(p2)
    print(p3)
    print(p4)
    #print so pra debugar

    team1 = biggerCard(p1,p3)
    if team1 == 0:
        #verifica se houve empate dentro do time
        team1 = p1

    team2 = biggerCard(p2,p4)
    if team2 == 0:
        team2 = p2

    winner = biggerCard(team1,team2)
    # verifica se houve impate entre os times e determina o ganhador
    if winner == 0:
        print ("Empate!!!")
    elif winner == team1:
        print("Team1 wins")
    elif winner == team2:
        print("Team2 wins")



def biggerCard(a,b):
    #retorna a maior das duas cartas
    numbers = ['4','5','6','7','Q','J','K','A','2','3']
    # todos os elementos da lista foram mudados pra char, pra diminuir a chance de erro

    for i in range(0,10):
        if a[0] == numbers[i]:
            p1 = i
    # estes dois loops determinam qual o index da carta escolhida
    # cartas mais fortes possuem index maior, ex: o index do As eh: 7, maior que o index da Queen: 4
    for i in  range(0,10):
        if b[0] == numbers[i]:
            p2 = i

    if p1 > p2:
        return a
    elif p1 < p2:
        return b
    elif p1 == p2:
        return 0    
    



def jumpLine():
    print('')

# falta comparar os valores pra ver quem ganha


list = []

welcomeTruco()
gameMatch()

print(list)
